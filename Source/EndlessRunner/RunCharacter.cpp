// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "RunCharacterController.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(RootComponent);
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	isDead = false;
	Coins = 0;
}

void ARunCharacter::AddCoin()
{
	Coins++;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::FromInt(Coins));
}

void ARunCharacter::Die()
{
	if (!isDead)
	{
		isDead = true;
		GetController()->DisableInput(Cast<ARunCharacterController>(GetController()));
		GetMesh()->SetVisibility(false);
		OnDeath.Broadcast(this);
	}
	
}

