// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExitedSignature, ATile*, Tile);

UCLASS()
class ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();
	
	UPROPERTY(BlueprintAssignable)
	FExitedSignature Exited;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray <TSubclassOf<class AObstacle>> ObstacleTypes;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray <TSubclassOf<class APickUp>> PickupTypes;
	
protected:
	UPROPERTY(VisibleAnywhere)
		class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UArrowComponent* AttachPoint;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UBoxComponent* ExitTrigger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* ObstacleSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* PickupSpawn;
	
	FVector SpawnPoint;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// BeginOverlap
	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

	FVector GetAttachTransform();
	
private:
		void SpawnObstacle();
		void SpawnPickup();
};
