// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUp.generated.h"

UCLASS()
class ENDLESSRUNNER_API APickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUp();

protected:
	UPROPERTY(VisibleAnywhere)
		class USceneComponent* SceneComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UPointLightComponent* PointLight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class URotatingMovementComponent* RotatingMovement;
	
public:	
	UFUNCTION(BlueprintImplementableEvent)
		void OnGet(ARunCharacter* RunCharacter);
	UFUNCTION()
		void OnCompHit(UPrimitiveComponent* HitComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			FVector NormalImpulse,
			const FHitResult& SweepResult);

};
