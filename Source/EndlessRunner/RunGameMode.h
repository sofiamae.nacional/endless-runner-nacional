// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public: 
	FVector AttachTransform = FVector(0);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATile> Tile;

	virtual void BeginPlay() override;
	UFUNCTION()
		void SpawnTiles();
	UFUNCTION()
		void OnTileExited(ATile* TileExited);
	UFUNCTION()
		void DestroyTile(ATile* TileToDestroy);

	FTimerHandle TimerHandle;
};
