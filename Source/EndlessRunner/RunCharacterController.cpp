// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "Components/InputComponent.h"
#include "GameFramework/PawnMovementComponent.h"

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	RunCharacter = Cast<ARunCharacter>(GetPawn());
	Movement = RunCharacter->GetMovementComponent();
}

void ARunCharacterController::MoveRight(float scale)
{
	Movement->AddInputVector(RunCharacter->GetActorRightVector() * scale);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Movement->AddInputVector(RunCharacter->GetActorForwardVector() * 20 * DeltaTime);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}
