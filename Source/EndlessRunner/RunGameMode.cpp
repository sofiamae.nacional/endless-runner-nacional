// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "RunCharacter.h"
#include "Tile.h"
#include "Obstacle.h"
#include "Engine/World.h"
#include "TimerManager.h"

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < 5; i++)
	{
		SpawnTiles();
	}

}

void ARunGameMode::SpawnTiles()
{
	FActorSpawnParameters SpawnParam;
	ATile* TileRef = GetWorld()->SpawnActor<ATile>(Tile, AttachTransform, FRotator(0), SpawnParam);
	AttachTransform = TileRef->GetAttachTransform() + FVector(500, 0, 0);

	TileRef->Exited.AddDynamic(this, &ARunGameMode::OnTileExited);
}

void ARunGameMode::OnTileExited(ATile* TileExited)
{
	SpawnTiles();
	GetWorldTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTile, TileExited), 0.5f, false);
}

void ARunGameMode::DestroyTile(ATile* TileToDestroy)
{
	TArray<AActor*> Obstacles;
	TileToDestroy->GetAttachedActors(Obstacles);

	for (int i = 0; i < Obstacles.Num(); i++) 
	{
		Obstacles[i]->Destroy();
	}

	TileToDestroy->Destroy();
}
