// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterPawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ARunCharacterPawn::ARunCharacterPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void ARunCharacterPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARunCharacterPawn::MoveForward(float scale)
{
}

void ARunCharacterPawn::MoveRight(float scale)
{
}

// Called every frame
void ARunCharacterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunCharacterPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoverForward", this, &ARunCharacterPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoverForward", this, &ARunCharacterPawn::MoveRight);
}

