// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "PickUp.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);
	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(RootComponent);
	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(RootComponent);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::BeginOverlap);

	// Obstacle Spawn
	ObstacleSpawn = CreateDefaultSubobject<UBoxComponent>(TEXT("ObstacleSpawn"));
	ObstacleSpawn->SetupAttachment(RootComponent);
	PickupSpawn = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupSpawn"));
	PickupSpawn->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	if(UKismetMathLibrary::RandomBoolWithWeight(0.6f))
		SpawnObstacle();
	else if(UKismetMathLibrary::RandomBoolWithWeight(0.3f))
		SpawnPickup();
	
}

void ATile::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* RunChar = Cast<ARunCharacter>(OtherActor)) 
	{
		Exited.Broadcast(this);
	}
}

FVector ATile::GetAttachTransform()
{
	return AttachPoint->GetComponentLocation();
}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters SpawnParam;
	SpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox
	(ObstacleSpawn->GetComponentLocation(), ObstacleSpawn->GetScaledBoxExtent());
	
	AObstacle* ObstacleRef = GetWorld()->SpawnActor<AObstacle>(ObstacleTypes[0], SpawnPoint, FRotator(0), SpawnParam);
	ObstacleRef->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void ATile::SpawnPickup()
{
	FActorSpawnParameters SpawnParam;
	SpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox
	(PickupSpawn->GetComponentLocation(), PickupSpawn->GetScaledBoxExtent());

	APickUp* PickupRef = GetWorld()->SpawnActor<APickUp>(PickupTypes[0], SpawnPoint, FRotator(0), SpawnParam);
	PickupRef->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

